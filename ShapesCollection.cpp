#include "ShapesCollection.h"
#include "RapidXMLReader.h"

using namespace mg;

// OpenGL Context Settings - this is a Global variable!!!
sf::ContextSettings gl_settings;


ShapesCollection::ShapesCollection() {}

// add shapes manually
void ShapesCollection::add_shape(const Polygon shape)
{
	_collection.push_back(shape);
}

void ShapesCollection::read_svg(const std::string& filename)
{
	if (_collection.empty()) {
		RapidXMLReader reader(filename);
		reader.parse(reader.get_root_node(), parseShape, &_collection);
	}
	else 
		throw "Reading shapes from a file is only possible for an empty collection!";

}

void ShapesCollection::clear()
{
	_collection.clear();
}

// display shapes in SFML window
void ShapesCollection::display(std::size_t width, 
							   std::size_t height,
							   std::size_t pixelBitDepth) const
{
	sf::RenderWindow window(sf::VideoMode(width, height, pixelBitDepth), "ShapesCollection");

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			//if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
			//	window.close();
		}

		window.clear();

		// render all shapes to the RenderWindow
		for (auto& shape: _collection)
			window.draw(shape);

		window.display();
	}

}


// TODO:
void ShapesCollection::save_image(std::string filename,
								  std::size_t width,
								  std::size_t height,
								  sf::ContextSettings settings) const
{
	sf::RenderTexture rtexture;
	rtexture.create(width, height, settings);
	rtexture.clear(sf::Color::Black);

	// render all shapes to the RenderTexture
	for (auto& shape: _collection)
		rtexture.draw(shape);

	rtexture.display();

	// save to file
	rtexture.getTexture().copyToImage().saveToFile(filename);

}

sf::Image shape::ShapesCollection::to_array(std::size_t width, std::size_t height, sf::ContextSettings settings) const
{
	sf::RenderTexture rtexture;
	rtexture.create(width, height, settings);
	rtexture.clear(sf::Color::Black);

	// render all shapes to the RenderTexture
	for (auto& shape : _collection)
		rtexture.draw(shape);

	rtexture.display();

	// save to file
	return	rtexture.getTexture().copyToImage();
}


// destructor
ShapesCollection::~ShapesCollection() {}
