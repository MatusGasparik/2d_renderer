#pragma once
#include <vector>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "Shape.h"


// default ContextSettings
extern sf::ContextSettings gl_settings;

namespace mg {
	class ShapesCollection
	{
	public:
		// default constructor
		ShapesCollection();

		// manually add shapes
		void add_shape(const Polygon shape);

		// add shapes from a svg file (the collection must be initially empty)
		void read_svg(const std::string& filename);

		// number of elements in the collection 
		std::size_t size() { return _collection.size(); }

		// remove all objects from the collection
		void clear();

		// display in a SFML window
		void display(std::size_t width,
			std::size_t height,
			std::size_t pixel) const;

		// render and save to a bitmap
		void save_image(std::string filename,
			std::size_t width,
			std::size_t height,
			sf::ContextSettings settings = gl_settings) const;

		// render and save to a bitmap
		sf::Image to_array(std::size_t width, std::size_t height, sf::ContextSettings settings = gl_settings) const;

		// check if collection is empty
		bool empty() { return _collection.empty();}

		~ShapesCollection();

		std::vector<Polygon> get_collection() const {
			return _collection;
		}

	private:
		std::vector<Polygon> _collection;
	};
} // namespace mg

