#ifdef _DEBUG
	#include "testfunctions.h"
#else
#include <iostream>
#include <vector>

#include <rapidxml.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "RapidXMLReader.h"
#include "ShapesCollection.h"


using namespace std;
using namespace rapidxml;




int main()
{
	//////////////////////////////////////////////////	
	// SVG: high level usage - ShapesCollection
	//////////////////////////////////////////////////	
	shape::ShapesCollection my_shapes;
	my_shapes.read_svg("vectorpaint.svg");

	// display on screen
	my_shapes.display(800, 600, 16);

	// save to a file
	//my_shapes.save_image("my_file1.png", 800, 600);
	sf::Image image = my_shapes.to_array(800, 600);


	system("pause");



	////////////////////////////////////////////////////	
	//// SVG: use XML reader
	////////////////////////////////////////////////////	
	//RapidXMLReader reader("vectorpaint.svg");    // "test.svg"
	//reader.print_doc();

	//vector<shape::Polygon> my_shapes2;
	//reader.parse(reader.get_root_node(), parseShape, &my_shapes2);

	//for (auto& x : my_shapes2) cout << x << endl;


	//system("pause");



	////////////////////////////////////////////////////	
	//// Create shapes manually
	////////////////////////////////////////////////////	
	//shape::Polygon triangle{ { 50.f, 50.f },{ 200.f, 330.f },{ 75.f, 430.f } };
	//cout << triangle;

	//my_shapes.clear();
	//my_shapes.add_shape(triangle);

	//// display on the screen
	//my_shapes.display(800, 600, 16);    

	//// save to a file
	//my_shapes.save_image("my_file2.png", 800, 600);



	////////////////////////////////////////////////////	
	//// Change the OpenGL context settings
	////////////////////////////////////////////////////	
	//gl_settings.antialiasingLevel = 12;
	//my_shapes.save_image("my_file3.png", 800, 600);


	//system("pause");
	return 0;

}

#endif // _DEBUG
