#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <regex>
#include "rapidxml_print.hpp"

#include "RapidXMLReader.h"


RapidXMLReader::RapidXMLReader(const std::string filename)
	: xmlFile(filename.c_str())
{
	doc.parse<0>(xmlFile.data());
	root_node = doc.first_node("svg");
}

// print the entire xml document
void RapidXMLReader::print_doc() const 
{
	std::cout << doc << std::endl;
}

// this is a recursive function!!!
void RapidXMLReader::parse(const rapidxml::xml_node<>* node, ParseFuncPtr func, std::vector<shape::Polygon>* collection) const
{
	func(node, collection);

	rapidxml::xml_node<>* next_node;
	for (next_node=node->first_node(); next_node; next_node = next_node->next_sibling()) {
		if (next_node->first_node()) {
			parse(next_node, func, collection);
		}
		else {
			func(next_node, collection);
		}
	}

}

// destructor
RapidXMLReader::~RapidXMLReader() {}



// get node's attribute value by name
std::string getAttrValue(const rapidxml::xml_node<>* node, const std::string name)
{
	return node->first_attribute(name.c_str())->value();
}

void node_print(const rapidxml::xml_node<>* node)
{
	//std::cout << "NODE TYPE: " << node->type() << std::endl;

	if (node->type() == rapidxml::node_type::node_element) {
		std::cout << node->name();
		std::cout << " (";

		for (rapidxml::xml_attribute<>* attr = node->first_attribute(); attr; attr = attr->next_attribute())
			std::cout << attr->name() << ", ";
		std::cout << ")";
		
		std::cout << std::endl;

	}
}


void parseShape(const rapidxml::xml_node<>* node, std::vector<shape::Polygon>* collection)
{
	if (node->type() == rapidxml::node_type::node_element) {

		std::string nodeName = node->name();

		if (nodeName == "rect") {
			float x = std::stof(getAttrValue(node, "x"));
			float y = std::stof(getAttrValue(node, "y"));
			float w = std::stof(getAttrValue(node, "width"));
			float h = std::stof(getAttrValue(node, "height"));

			shape::Polygon rect{
				{x, y},
				{x + w, y},
				{x + w, y + h},
				{x, y + h}
			};

			collection->push_back(rect);
			std::cout << "rect: " << rect << std::endl;
		}

		else if (nodeName == "path") {
			shape::Polygon poly = parse_SVGPath(getAttrValue(node, "d"));
			collection->push_back(poly);
			std::cout << "path: " << poly << std::endl;

		}
	}
}

std::vector<float> get_floats_from_string(const std::string & str)
{
	std::vector<float> coords;

	std::regex pattern("[+-]?([\\d]*\\.)?[\\d]+");
	std::sregex_iterator next(str.begin(), str.end(), pattern);
	std::sregex_iterator end;

	float val;

	while (next != end) {
		val = std::stof(next->str());
		coords.push_back(val);

		++next;
	}

	return coords;
}

void apply_svg_path_M_cmd(const bool isupper, const std::vector<float>& coords, shape::Polygon& poly)
{
	sf::Vector2f vertex(coords[0], coords[1]);
	size_t index = poly.getPointCount();

	// this is the first path command, polygon object must be empty
	if (index != 0)
		throw "Cannot apply svg-path M command on polygon with non-zero point count!";

	if (isupper) {
			poly.setPointCount(index + 1);
			poly.setPoint(index, vertex);
	}

	else {
		throw "Not Implemented";
		// code
	}
}


void apply_svg_path_L_cmd(const bool isupper, const std::vector<float>& coords, shape::Polygon& poly)
{
	sf::Vector2f vertex(coords[0], coords[1]);
	size_t index = poly.getPointCount();

	// polygon must have at least one vertex
	if (index == 0)
		throw "Cannot apply svg-path L command on an empty polygon";

	if (isupper) {
			poly.setPointCount(index + 1);
			poly.setPoint(index, vertex);
	}

	else {
		throw "Not Implemented";
		// code
	}
}


void apply_svg_path_C_cmd(const bool isupper, const std::vector<float>& coords, shape::Polygon& poly)
{
	// only get the endpoint coords (last 2 of 6) => convert curve to line
	sf::Vector2f vertex(coords[4], coords[5]);
	size_t index = poly.getPointCount();

	// polygon must have at least one vertex
	if (index == 0)
		throw "Cannot apply svg-path L command on an empty polygon";

	if (isupper) {
			poly.setPointCount(index + 1);
			poly.setPoint(index, vertex);
	}

	else {
		throw "Not Implemented";
		// code
	}
}



void apply_svg_path_subcmd(const char cmd, const std::vector<float>& coords, shape::Polygon& poly)
{
	switch (cmd)
	{
	case 'm':
	case 'M': apply_svg_path_M_cmd(isupper(cmd), coords, poly); break;

	case 'l':
	case 'L': apply_svg_path_L_cmd(isupper(cmd), coords, poly); break;

	case 'c':
	case 'C': apply_svg_path_C_cmd(isupper(cmd), coords, poly); break;

	default:
		break;
	}

}


// parse the svg path using regex
shape::Polygon parse_SVGPath(const std::string& path_string) {

	shape::Polygon poly;
	
	std::regex pattern("([a-zA-Z])([.,\\d\\s]+)|([zZ])$");
	std::sregex_iterator next(path_string.begin(), path_string.end(), pattern);
	std::sregex_iterator end;
	std::smatch match;

	std::string cmd;
	std::vector<float> coords;

	while (next != end) {
		match = *next;

		if (match[0] == "Z" || match[0] == "z")  // here, entire match == first group (match[0])
		{
			break;
		}
		cmd = match[1];
		coords = get_floats_from_string(match[2]);
		apply_svg_path_subcmd(cmd[0], coords, poly);
		++next;
	}

	return poly;
}

