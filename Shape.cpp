#include "Shape.h"

std::ostream & operator<<(std::ostream & os, shape::Polygon & poly)
{
	return os << "Polygon(n_vertices=" << poly.getPointCount() << ")" << std::endl;
}
