# 2D Renderer
The code in this repository is the final project I made for my C++ course. 
It was meant as a learning project with the main objective to learn the OOP concepts in C++.

# TODO
I developed this code in Visual Studio. Next, I want to use CMake to make it possible to 
build on Linux/Unix or macOS. It should be used as a static library
and eventually be called from Python with a wrapper written in Cython.
