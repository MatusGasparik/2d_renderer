#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>

namespace mg {

	class Polygon : public sf::ConvexShape {
	public:
		// default constructor 
		Polygon() : sf::ConvexShape() {}   // re-use the default constructor from the parent class

		// general constructor
		Polygon(std::initializer_list<sf::Vector2f> vertices) {

			int num_vertices = vertices.size();
			setPointCount(num_vertices);

			int index = 0;
			for (const sf::Vector2f* point = vertices.begin(); point != vertices.end(); point++) {
				setPoint(index, *point);
				index++;
			}
		}
	};


}  // namespace mg

std::ostream& operator<<(std::ostream& os, shape::Polygon& poly);



