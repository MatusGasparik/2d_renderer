
#ifdef _DEBUG

#include "testfunctions.h"
#include "catch.hpp"


TEST_CASE("Testing Polygon class with default constructor", "[polygon]") {

	shape::Polygon shape;

	SECTION("Setting the correct number or vertices beforehand") {
		shape.setPointCount(5);

		shape.setPoint(0, { 20.f, 20.f });
		shape.setPoint(1, { 50.f, 20.f });
		shape.setPoint(2, { 60.f, 40.f });
		shape.setPoint(3, { 35.f, 60.f });
		shape.setPoint(4, { 10.f, 40.f });

		REQUIRE(shape.getPointCount() == 5);

		// w/o transformations global bounds == minimal bounding box
		REQUIRE(shape.getGlobalBounds().left == 10.f);
		REQUIRE(shape.getGlobalBounds().top == 20.f);
		REQUIRE(shape.getGlobalBounds().width == 50.f);
		REQUIRE(shape.getGlobalBounds().height == 40.f);
	}

	SECTION("Setting arbitrarily large number of vertices") {
		shape.setPointCount(1000);

		shape.setPoint(0, { 20.f, 20.f });
		shape.setPoint(1, { 50.f, 20.f });
		shape.setPoint(2, { 60.f, 40.f });
		shape.setPoint(3, { 35.f, 60.f });
		shape.setPoint(4, { 10.f, 40.f });

		SECTION("...and forgetting to set the correct number afterwards") {
			CHECK_FALSE(shape.getPointCount() == 5);
			CHECK_FALSE(shape.getGlobalBounds().left == 10.f);
			CHECK_FALSE(shape.getGlobalBounds().top == 20.f);
			CHECK_FALSE(shape.getGlobalBounds().width == 50.f);
			CHECK_FALSE(shape.getGlobalBounds().height == 40.f);

		}
		SECTION("...and setting the correct number afterwards") {
			shape.setPointCount(5);

			CHECK(shape.getPointCount() == 5);
			CHECK(shape.getGlobalBounds().left == 10.f);
			CHECK(shape.getGlobalBounds().top == 20.f);
			CHECK(shape.getGlobalBounds().width == 50.f);
			CHECK(shape.getGlobalBounds().height == 40.f);

		}

	}

}


TEST_CASE("Testing Polygon class with general constructor", "[polygon]") {

	shape::Polygon shape{
		{ 20.f, 20.f },
		{ 50.f, 20.f },
		{ 60.f, 40.f },
		{ 35.f, 60.f },
		{ 10.f, 40.f }
	};

	REQUIRE(shape.getPointCount() == 5);

	REQUIRE(shape.getGlobalBounds().left == 10.f);
	REQUIRE(shape.getGlobalBounds().top == 20.f);
	REQUIRE(shape.getGlobalBounds().width == 50.f);
	REQUIRE(shape.getGlobalBounds().height == 40.f);

}


TEST_CASE("Testing ShapesCollection class", "[shapescollection]") {

	shape::ShapesCollection collection;
	shape::Polygon shape{
		{ 20.f, 20.f },
		{ 50.f, 20.f },
		{ 60.f, 40.f },
		{ 35.f, 60.f },
		{ 10.f, 40.f }
	};

	SECTION("Collection must be empty after initilization") {
		CHECK(collection.empty());
	}

	SECTION("Collection is not empty after shape added") {
		collection.add_shape(shape);
		CHECK_FALSE(collection.empty());

		SECTION("Checking collection empty after calling clear()") {
			collection.clear();
			CHECK(collection.empty());
		}
	}

	SECTION("Reading shapes from an SVG file") {
		collection.read_svg("vectorpaint.svg");
		CHECK_FALSE(collection.empty());

		SECTION("Reading shapes from an SVG file when collection not empty") {
			CHECK_THROWS(collection.read_svg("test.svg"));
		}
	}
}

TEST_CASE("Testing the XML/SVG Reader", "[XMLReader]") {

	SECTION("Testing parsing a SVG path string (d=...)") {

		SECTION("Testing string 1") {
			std::string path_string = "M 100 100 L 150 100 L 150 150 Z"; // a triangle
			shape::Polygon shape = parse_SVGPath(path_string);

			INFO("path string = " << path_string);
			CHECK(shape.getPointCount() == 3);
		}

		SECTION("Testing string 2") {
			std::string path_string = "M 100,100 L 150,100 L 150,150  Z"; // a triangle
			shape::Polygon shape = parse_SVGPath(path_string);

			INFO("path string = " << path_string);
			CHECK(shape.getPointCount() == 3);

		}
	}
}

#endif // !_DEBUG
