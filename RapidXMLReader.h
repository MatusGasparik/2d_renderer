#pragma once
#include <string>
#include <iostream>
#include <vector>

#include "rapidxml.hpp"
#include "rapidxml_utils.hpp"

#include "Shape.h"


////////////////////////////////////////////////////////////////////////////////
//  TYPEDEFS
////////////////////////////////////////////////////////////////////////////////
typedef void(*ParseFuncPtr)(const rapidxml::xml_node<>* node, std::vector<shape::Polygon>* collection);


////////////////////////////////////////////////////////////////////////////////
// SVG/XML Reader class
////////////////////////////////////////////////////////////////////////////////
class RapidXMLReader
{
public:
	// don't allow default constructor
	RapidXMLReader() = delete;

	// general constructor
	RapidXMLReader(const std::string filename);

	// print the entire xml
	void print_doc() const;

	// parse the elements of the xml tree using a supplied parsing function
	void parse(const rapidxml::xml_node<>* root_node, ParseFuncPtr func, std::vector<shape::Polygon>* collection) const;

	rapidxml::xml_node<>* get_root_node() const { return root_node; };

	~RapidXMLReader();

private:
	rapidxml::file<> xmlFile;
	rapidxml::xml_document<> doc;
	rapidxml::xml_node<>* root_node;
};


////////////////////////////////////////////////////////////////////////////////
// XML NODE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
std::string getAttrValue(const rapidxml::xml_node<>* node, const std::string name);

void node_print(const rapidxml::xml_node<>* node);

void parseShape(const rapidxml::xml_node<>* node, std::vector<shape::Polygon>* collection);


////////////////////////////////////////////////////////////////////////////////
// helper funcs for parsing the svg-path's "d" data
////////////////////////////////////////////////////////////////////////////////

// get all numeric values from a string
std::vector<float> get_floats_from_string(const std::string& str);

// SVG path commands (M|m, L|l, C|c, ..., Z|z)
void apply_svg_path_M_cmd(const bool isupper, const std::vector<float>& coords, shape::Polygon& poly);
void apply_svg_path_L_cmd(const bool isupper, const std::vector<float>& coords, shape::Polygon& poly);
void apply_svg_path_C_cmd(const bool isupper, const std::vector<float>& coords, shape::Polygon& poly);

// apply a single svg path subsommand (adding a vertex to the Polygon object)
void apply_svg_path_subcmd(const char cmd, const std::vector<float>& coords, shape::Polygon& poly);

shape::Polygon parse_SVGPath(const std::string& path);
